angular.module('myApp')
  .controller('Hours', hours);

function hours() {
  var vm = this;

  vm.startTime = '08:00';
  vm.startLunchTime = '12:00';
  vm.endTime = '18:00';
  vm.variation = 5;
  vm.days = 1;

  vm.workingHours = [];

  vm.generate = generate;

  /////////////

  function generate() {
    vm.workingHours = [];
    var minuteStart = pegaOsMinutosDaString(vm.startTime);
    var minuteLunchStart = pegaOsMinutosDaString(vm.startLunchTime);
    var variationMin = parseInt();

    var variationMinutes = [];
    var variationLunchMinutes = [];

    // generate variations
    variationMinutes = generateVariationMinutes(minuteStart);
    variationLunchMinutes = generateVariationMinutes(minuteLunchStart);

    for (var i = 1; i <= vm.days; i++) {
      var day = {};

      // generate startWorkTime and endWorkTime
      startHour = pegaAsHorasDaString(vm.startTime);
      endHour = pegaAsHorasDaString(vm.endTime);
      day.work = generateHours(minuteStart, variationMinutes, startHour, endHour);

      // generate startLunchTime and endLunchTime
      startLunchHour = parseInt(vm.startLunchTime.substr(0, vm.startLunchTime.indexOf(":")));
      endLunchHour = startLunchHour + 1;
      day.lunch = generateHours(minuteStart, variationLunchMinutes, startLunchHour, endLunchHour);

      vm.workingHours.push(day);
    }
  }

  function generateVariationMinutes(minuteStart) {
    var variation = vm.variation;
    var variationMinutes = [];

    for (var i = (minuteStart - variation); i <= (minuteStart + variation); i++){
      variationMinutes.push(i);
    }

    return variationMinutes;
  }

  function generateHours(minuteStart, variationMinutes, startHour, endHour) {
    var hours = {};

    // work on startMinute and startHour
    var startMinute = variationMinutes[Math.floor(Math.random() * variationMinutes.length)];

    if (startMinute < 0) {
      startMinute = startMinute + 60;
      startHour -= 1;
    }

    startHour = mantemDuasCasasDecimais(startHour);
    startMinute = mantemDuasCasasDecimais(startMinute);

    // work on endMinute and endHour
    endMinute = Math.floor(Math.random() * (minuteStart + vm.variation));
    if (endMinute < 60) {
      endHour -= 1;
      if (startMinute >= (30 - vm.variation)) {
        endHour += 1 ;
      } else if (endMinute.between(0, vm.variation * 2, true)) {
        endHour += 1 ;
      }
    } else if (endMinute == 60) {
      endMinute = 0;
    } else if (endMinute > 60) {
      endMinute = (endMinute - 60);
    }

    endHour = mantemDuasCasasDecimais(endHour);
    endMinute = mantemDuasCasasDecimais(endMinute);

    hours.start = startHour.valueOf() + ':' + startMinute.valueOf();
    hours.end = endHour.valueOf() + ':' + endMinute.valueOf();

    return hours;
  }

  function mantemDuasCasasDecimais(numero) {
    return numero < 10 ? '0' + numero : numero;
  }

  function pegaOsMinutosDaString(hora) {
    return parseInt(hora.substr(hora.indexOf(":") + 1));
  }

  function pegaAsHorasDaString(hora) {
    return parseInt(hora.substr(0, hora.indexOf(":")));
  }

  Number.prototype.between = function(a, b, inclusive) {
    var min = Math.min.apply(Math, [a, b]),
    max = Math.max.apply(Math, [a, b]);
    return inclusive ? this >= min && this <= max : this > min && this < max;
  }
}
